# K3Y SANCUS PFCP Intrusion Detection System - UC1.2/UC1.9 Attacks


## Introduction

The **PFCP Network Intrusion Detection System (PFCP_IDS_Sensor)** is a Python program designed to detect potential attacks in Packet Forwarding Control Protocol (PFCP) network traffic. PFCP is a protocol used in 5G mobile networks to manage and control user data flows between the User Equipment (UE) and the Data Network (DN). This IDS sensor is capable of detecting various types of attacks, such as PFCP Session Deletion Flood DoS Attack, PFCP Session Establishment DoS Attack, and PFCP Session Modification Attacks.

## Installation

Pull the docker image:

`sudo docker pull registry.gitlab.com/k3y/pfcp-ids:latest`

The docker image should now be available locally.


## Usage

The PFCP_IDS_Sensor can be operated easily as a docker container. In order to pass pcap files to the container, you can either copy them inside the container or alternatively you can mount the entire directory from the host machine. The watchdog of the sensor should pick up any additions to said directory.


### Docker

Launch the docker container as follows:

`sudo docker run -it registry.gitlab.com/k3y/pfcp-ids:latest PFCP_TCP_IDS_Sensor.py <kafka_topic> <kafka_server:kafka_port> <pcap_dir_abs>`

For example:

`sudo docker run -it registry.gitlab.com/k3y/pfcp-ids:latest test-topic localhost:9092 /app/bin/pcaps`

*NOTE*: You need to specify an ABSOLUTE path for the watchdog directory.

*NOTE*: The absolute path for the watchdog directory is by default internal to the docker container created by this docker image. Feel free to copy pcap files to the container or to mount an entire directory.

The program will hang upon launching, until a new pcap file becomes present in the psecified directory. Assuming this happens, the output will resemble:

```
Monitoring directory: /home/ampo/SANCUS_PFCP_IDS_Sensor/CICFlowMeter-4.0/bin/pcaps
New pcap file was created: /home/ampo/SANCUS_PFCP_IDS_Sensor/CICFlowMeter-4.0/bin/pcaps/pfcp88.pcap

--------------
PFCP TCP/IP Network Flow Statistics Generation

cic.cs.unb.ca.ifm.Cmd You select: /home/ampo/SANCUS_PFCP_IDS_Sensor/CICFlowMeter-4.0/bin/pcaps/pfcp88.pcap
cic.cs.unb.ca.ifm.Cmd Out folder: /home/ampo/SANCUS_PFCP_IDS_Sensor/CICFlowMeter-4.0/bin/pcaps
cic.cs.unb.ca.ifm.Cmd CICFlowMeter received 1 pcap file
Working on... pfcp88.pcap
pfcp88.pcap is done. total 404 flows 
Packet stats: Total=199111,Valid=194938,Discarded=4173
-------------------------------------------------------------------------------
New PFCP Flows were generated: /home/ampo/SANCUS_PFCP_IDS_Sensor/CICFlowMeter-4.0/bin/pcaps/pfcp88.pcap_Flows.csv
/home/ampo/.local/lib/python3.8/site-packages/sklearn/base.py:409: UserWarning: X does not have valid feature names, but StandardScaler was fitted with feature names
  warnings.warn(

--------------
PFCP TCP/IP Flow #1 | Detection: Normal
/home/ampo/.local/lib/python3.8/site-packages/sklearn/base.py:409: UserWarning: X does not have valid feature names, but StandardScaler was fitted with feature names
  warnings.warn(

--------------
PFCP TCP/IP Flow #2 | Detection: Normal
/home/ampo/.local/lib/python3.8/site-packages/sklearn/base.py:409: UserWarning: X does not have valid feature names, but StandardScaler was fitted with feature names
  warnings.warn(

--------------
PFCP TCP/IP Flow #3 | Detection: PFCP Session Deletion Flood DoS Attack
/home/ampo/.local/lib/python3.8/site-packages/sklearn/base.py:409: UserWarning: X does not have valid feature names, but StandardScaler was fitted with feature names
  warnings.warn(

--------------
PFCP TCP/IP Flow #4 | Detection: PFCP Session Deletion Flood DoS Attack
/home/ampo/.local/lib/python3.8/site-packages/sklearn/base.py:409: UserWarning: X does not have valid feature names, but StandardScaler was fitted with feature names
  warnings.warn(

--------------
```

## Kafka

Launching a kafka consumer and printing the received messages from the sensor will yield similar results to the output below.

```
[10, 100, "eth0", 1696353863.465819, 114, "detected", "attack", "Transmission of malicious PFCP session deletion control messages targetting the UPF, which handles processes and forwards user data to the DN. The goal of this attack is to disconnect a User Equipment from the DN.", {"event_68": {"timestamp": 1696353863.465819, "description": "Transmission of malicious PFCP session deletion control messages targetting the UPF, which handles processes and forwards user data to the DN. The goal of this attack is to disconnect a User Equipment from the DN.", "attributes": [["Flow ID", "172.21.0.110-172.21.0.120-8805-8805-17"], ["Src IP", "172.21.0.110"], ["Src Port", "8805"], ["Dst IP", "172.21.0.120"], ["Dst Port", "8805"], ["Protocol", "17"], ["Timestamp", "1696353863.465819"], ["Flow Duration", "110012432"], ["Tot Fwd Pkts", "21"], ["Tot Bwd Pkts", "23"], ["TotLen Fwd Pkts", "336.0"], ["TotLen Bwd Pkts", "368.0"], ["Fwd Pkt Len Max", "16.0"], ["Fwd Pkt Len Min", "16.0"], ["Fwd Pkt Len Mean", "16.0"], ["Fwd Pkt Len Std", "0.0"], ["Bwd Pkt Len Max", "16.0"], ["Bwd Pkt Len Min", "16.0"], ["Bwd Pkt Len Mean", "16.0"], ["Bwd Pkt Len Std", "0.0"], ["Flow Byts/s", "6.399276765375026"], ["Flow Pkts/s", "0.3999547978359391"], ["Flow IAT Mean", "2558428.6511627906"], ["Flow IAT Std", "4702325.258349067"], ["Flow IAT Max", "11001189.0"], ["Flow IAT Min", "71.0"], ["Fwd IAT Tot", "110011988.0"], ["Fwd IAT Mean", "5500599.4"], ["Fwd IAT Std", "5642999.0311195655"], ["Fwd IAT Max", "11001189.0"], ["Fwd IAT Min", "369.0"], ["Bwd IAT Tot", "110012339.0"], ["Bwd IAT Mean", "5000560.863636364"], ["Bwd IAT Std", "5606628.365948871"], ["Bwd IAT Max", "11001635.0"], ["Bwd IAT Min", "71.0"], ["Fwd PSH Flags", "0"], ["Bwd PSH Flags", "0"], ["Fwd URG Flags", "0"], ["Bwd URG Flags", "0"], ["Fwd Header Len", "168"], ["Bwd Header Len", "184"], ["Fwd Pkts/s", "0.1908875171489709"], ["Bwd Pkts/s", "0.2090672806869681"], ["Pkt Len Min", "16.0"], ["Pkt Len Max", "16.0"], ["Pkt Len Mean", "16.0"], ["Pkt Len Std", "0.0"], ["Pkt Len Var", "0.0"], ["FIN Flag Cnt", "0"], ["SYN Flag Cnt", "0"], ["RST Flag Cnt", "0"], ["PSH Flag Cnt", "0"], ["ACK Flag Cnt", "0"], ["URG Flag Cnt", "0"], ["CWE Flag Count", "0"], ["ECE Flag Cnt", "0"], ["Down/Up Ratio", "1.0"], ["Pkt Size Avg", "16.363636363636363"], ["Fwd Seg Size Avg", "16.0"], ["Bwd Seg Size Avg", "16.0"], ["Fwd Byts/b Avg", "0"], ["Fwd Pkts/b Avg", "0"], ["Fwd Blk Rate Avg", "0"], ["Bwd Byts/b Avg", "0"], ["Bwd Pkts/b Avg", "0"], ["Bwd Blk Rate Avg", "0"], ["Subflow Fwd Pkts", "21"], ["Subflow Fwd Byts", "336"], ["Subflow Bwd Pkts", "23"], ["Subflow Bwd Byts", "368"], ["Init Fwd Win Byts", "-1"], ["Init Bwd Win Byts", "-1"], ["Fwd Act Data Pkts", "21"], ["Fwd Seg Size Min", "0"], ["Active Mean", "481.4"], ["Active Std", "94.4201014379648"], ["Active Max", "693.0"], ["Active Min", "369.0"], ["Idle Mean", "11000714.5"], ["Idle Std", "315.4056470996019"], ["Idle Max", "11001189.0"], ["Idle Min", "11000258.0"], ["Label", "PFCP Session Deletion Flood DoS Attack"]]}}]

[10, 100, "eth0", 1696353863.6240773, 114, "detected", "attack", "Transmission of malicious PFCP session deletion control messages targetting the UPF, which handles processes and forwards user data to the DN. The goal of this attack is to disconnect a User Equipment from the DN.", {"event_71": {"timestamp": 1696353863.6240773, "description": "Transmission of malicious PFCP session deletion control messages targetting the UPF, which handles processes and forwards user data to the DN. The goal of this attack is to disconnect a User Equipment from the DN.", "attributes": [["Flow ID", "172.21.0.110-172.21.0.120-8805-8805-17"], ["Src IP", "172.21.0.110"], ["Src Port", "8805"], ["Dst IP", "172.21.0.120"], ["Dst Port", "8805"], ["Protocol", "17"], ["Timestamp", "1696353863.6240773"], ["Flow Duration", "110013899"], ["Tot Fwd Pkts", "21"], ["Tot Bwd Pkts", "23"], ["TotLen Fwd Pkts", "336.0"], ["TotLen Bwd Pkts", "368.0"], ["Fwd Pkt Len Max", "16.0"], ["Fwd Pkt Len Min", "16.0"], ["Fwd Pkt Len Mean", "16.0"], ["Fwd Pkt Len Std", "0.0"], ["Bwd Pkt Len Max", "16.0"], ["Bwd Pkt Len Min", "16.0"], ["Bwd Pkt Len Mean", "16.0"], ["Bwd Pkt Len Std", "0.0"], ["Flow Byts/s", "6.399191433075197"], ["Flow Pkts/s", "0.3999494645671998"], ["Flow IAT Mean", "2558462.7674418604"], ["Flow IAT Std", "4702414.36763572"], ["Flow IAT Max", "11001240.0"], ["Flow IAT Min", "42.0"], ["Fwd IAT Tot", "110013389.0"], ["Fwd IAT Mean", "5500669.45"], ["Fwd IAT Std", "5643126.301696348"], ["Fwd IAT Max", "11001240.0"], ["Fwd IAT Min", "308.0"], ["Bwd IAT Tot", "110013810.0"], ["Bwd IAT Mean", "5000627.727272727"], ["Bwd IAT Std", "5606720.246120587"], ["Bwd IAT Max", "11001712.0"], ["Bwd IAT Min", "42.0"], ["Fwd PSH Flags", "0"], ["Bwd PSH Flags", "0"], ["Fwd URG Flags", "0"], ["Bwd URG Flags", "0"], ["Fwd Header Len", "168"], ["Bwd Header Len", "184"], ["Fwd Pkts/s", "0.1908849717252544"], ["Bwd Pkts/s", "0.2090644928419453"], ["Pkt Len Min", "16.0"], ["Pkt Len Max", "16.0"], ["Pkt Len Mean", "16.0"], ["Pkt Len Std", "0.0"], ["Pkt Len Var", "0.0"], ["FIN Flag Cnt", "0"], ["SYN Flag Cnt", "0"], ["RST Flag Cnt", "0"], ["PSH Flag Cnt", "0"], ["ACK Flag Cnt", "0"], ["URG Flag Cnt", "0"], ["CWE Flag Count", "0"], ["ECE Flag Cnt", "0"], ["Down/Up Ratio", "1.0"], ["Pkt Size Avg", "16.363636363636363"], ["Fwd Seg Size Avg", "16.0"], ["Bwd Seg Size Avg", "16.0"], ["Fwd Byts/b Avg", "0"], ["Fwd Pkts/b Avg", "0"], ["Fwd Blk Rate Avg", "0"], ["Bwd Byts/b Avg", "0"], ["Bwd Pkts/b Avg", "0"], ["Bwd Blk Rate Avg", "0"], ["Subflow Fwd Pkts", "21"], ["Subflow Fwd Byts", "336"], ["Subflow Bwd Pkts", "23"], ["Subflow Bwd Byts", "368"], ["Init Fwd Win Byts", "-1"], ["Init Bwd Win Byts", "-1"], ["Fwd Act Data Pkts", "21"], ["Fwd Seg Size Min", "0"], ["Active Mean", "442.8"], ["Active Std", "77.89137735761686"], ["Active Max", "601.0"], ["Active Min", "308.0"], ["Idle Mean", "11000908.6"], ["Idle Std", "232.21742685089515"], ["Idle Max", "11001240.0"], ["Idle Min", "11000490.0"], ["Label", "PFCP Session Deletion Flood DoS Attack"]]}}]

```
